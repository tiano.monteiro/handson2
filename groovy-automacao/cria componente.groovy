import groovy.json.JsonSlurper
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

print "\n\n"
print "==========================================================="
print "\n"
print "\n Automacao para Criacao de recursos no UrbanCode Deploy"
print "\n"
println "\n===========================================================\n"

String serverUrl = "${p:serverUrl.name}"
String authString = "${p:authbase64}"
String recursoMatricula = "${p:matricula.name}"
String resourceTreeParentPath = "${p:parentPath.name}"
String agentName = "${p:agent.name}"
String compName = "${p:comp.name}"
String resourceTreeEnv = "${p:env.name}"
String templateName = "${p:template.name}"

def nullTrustManager = [
    checkClientTrusted: { chain, authType ->  },
    checkServerTrusted: { chain, authType ->  },
    getAcceptedIssuers: { null }
]
def nullHostnameVerifier = [
    verify: { hostname, session -> true }
]
SSLContext sc = SSLContext.getInstance("SSL")
sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes())


print "\n\n"
print "====================================================="
print "\nIniciando a criacao do componente a partir do template ..."
println "\n=================================================\n"

def content = """{"name": "${compName}", "description": "Componente ${compName}", "templateName": "${templateName}", "defaultVersionType": "FULL"}"""
def request = "${serverUrl}/cli/component/create";
def requestURL = new URL(request).openConnection();
requestURL.setRequestProperty("Authorization", "Basic " + authStringEnc);
requestURL.setDoOutput(true);
requestURL.setRequestMethod("PUT");
    if (content) {
        requestURL.setRequestProperty("Content-Type", "application/json");
        requestURL.setRequestProperty("Accept", "application/json");
        requestURL.connect();
        byte[] outputBytes = """${content}""".getBytes("UTF-8");
        OutputStream os = requestURL.getOutputStream();
        os.write(outputBytes);
        os.close();
    }
def requestRC = requestURL.getResponseCode();
    if (requestRC <= 299) {
        println "O componente ${compName} foi criado com sucesso."
    } else  {
        println "Nao foi possivel criar o componente ${compName}"
        System.exit(1)
    }
