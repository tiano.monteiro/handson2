import groovy.json.JsonSlurper
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

print "\n\n"
print "==========================================================="
print "\n"
print "\n Automacao para Criacao de recursos no UrbanCode Deploy"
print "\n"
println "\n===========================================================\n"

String serverUrl = "${p:serverUrl.name}"
String authString = "${p:authbase64}"
String recursoMatricula = "${p:matricula.name}"
String resourceTreeParentPath = "${p:parentPath.name}"
String agentName = "${p:agent.name}"
String applicationName = "${p:app.name}"
String compName = "${p:comp.name}"
String resourceTreeEnv = "${p:env.name}"
String templateName = "${p:template.name}"

def nullTrustManager = [
    checkClientTrusted: { chain, authType ->  },
    checkServerTrusted: { chain, authType ->  },
    getAcceptedIssuers: { null }
]
def nullHostnameVerifier = [
    verify: { hostname, session -> true }
]
SSLContext sc = SSLContext.getInstance("SSL")
sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes())


print "\n\n"
print "====================================================="
print "\n Criando o ambiente na aplicacao..."
println "\n=================================================\n"

def request = "${serverUrl}/cli/environment/createEnvironment?application=${applicationName}&name=${resourceTreeEnv}";
def requestURL = new URL(request).openConnection();
requestURL.setRequestProperty("Authorization", "Basic " + authStringEnc);
requestURL.setDoOutput(true);
requestURL.setRequestMethod("PUT");
def requestRC = requestURL.getResponseCode();
    if (requestRC <= 299) {
        println "O ambiente ${resourceTreeEnv} foi criado na aaplicacao ${applicationName} com sucesso."
    } else  {
        println "Nao foi possivel criar o ambiente ${resourceTreeEnv} na aplicacao ${applicationName}."
        System.exit(1)
    }
