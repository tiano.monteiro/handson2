import groovy.json.JsonSlurper
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

print "\n\n"
print "==========================================================="
print "\n"
print "\n Automacao para Criacao de recursos no UrbanCode Deploy"
print "\n"
println "\n===========================================================\n"

String serverUrl = "${p:serverUrl.name}"
String authString = "${p:authbase64}"
String recursoMatricula = "${p:matricula.name}"
String resourceTreeParentPath = "${p:parentPath.name}"
String agentName = "${p:agent.name}"
String applicationName = "${p:app.name}"
String compName = "${p:comp.name}"
String resourceTreeEnv = "${p:env.name}"
String templateName = "${p:template.name}"

def nullTrustManager = [
    checkClientTrusted: { chain, authType ->  },
    checkServerTrusted: { chain, authType ->  },
    getAcceptedIssuers: { null }
]
def nullHostnameVerifier = [
    verify: { hostname, session -> true }
]
SSLContext sc = SSLContext.getInstance("SSL")
sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes())


print "\n\n"
print "====================================================="
print "\n Adicionando o recurso base ao ambiente na aplicacao..."
println "\n=================================================\n"

def request = "${serverUrl}/cli/environment/addBaseResource?application=${applicationName}&environment=${resourceTreeEnv}&resource=${resourceTreeParentPath}/${recursoMatricula}/${resourceTreeEnv}";
def requestURL = new URL(request).openConnection();
requestURL.setRequestProperty("Authorization", "Basic " + authStringEnc);
requestURL.setDoOutput(true);
requestURL.setRequestMethod("PUT");
def requestRC = requestURL.getResponseCode();
    if (requestRC <= 299) {
        println "O recurso base ${resourceTreeParentPath}/${recursoMatricula}/${resourceTreeEnv} foi adicionado no ambiente ${resourceTreeEnv} da aplicacao ${applicationName} com sucesso."
    } else  {
        println "Nao foi possivel adicionar o recurso base ${resourceTreeParentPath}/${recursoMatricula}/${resourceTreeEnv} no ambiente ${resourceTreeEnv} da aplicacao ${applicationName}."
        System.exit(1)
    }
