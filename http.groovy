import groovy.json.JsonSlurper
import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

String gitUrl = "${p:component/git.url}"
String authString = "${p:component/gitAuth}"
String gitgroup = "${p:component/mygitgroup}"
String gitrepo = "${p:component/mygitrepo}"
String gitversion = "${p:version.name}"


def nullTrustManager = [
    checkClientTrusted: { chain, authType ->  },
    checkServerTrusted: { chain, authType ->  },
    getAcceptedIssuers: { null }
]
def nullHostnameVerifier = [
    verify: { hostname, session -> true }
]
SSLContext sc = SSLContext.getInstance("SSL")
sc.init(null, [nullTrustManager as X509TrustManager] as TrustManager[], null)
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
HttpsURLConnection.setDefaultHostnameVerifier(nullHostnameVerifier as HostnameVerifier)
String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes())


print "\n\n"
print "====================================================="
print "\n Download do arquivo Git Tag..."
println "\n=================================================\n"

def request = "https://${gitUrl}/${gitgroup}/${gitrepo}/repository/archive.zip?sha=${gitversion}";

def requestURL = new URL(request).openConnection();
requestURL.setRequestProperty("Authorization", "Basic " + authStringEnc);
requestURL.setDoOutput(true);
requestURL.setRequestMethod("GET");
def requestFile = new File("${gitrepo}-${gitversion}")
requestFile.bytes = requestURL.bytes

def requestRC = requestURL.getResponseCode();
    if (requestRC <= 299) {
        println "O download foi realizado com sucesso."
    } else  {
        println "Nao foi possivel realizar o download."
        System.exit(1)
    }
